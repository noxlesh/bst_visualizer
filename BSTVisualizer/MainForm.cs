﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace BSTVisualizer
{
    public partial class MainForm : Form
    {
        private BSTDraw tree;
        private int treeHeight;
        private int nodeCount;
        private float offset = 20F;
        public MainForm()
        {
            tree = new BSTDraw();
            InitializeComponent();
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            int val;
            if(Int32.TryParse(textNewVal.Text, out val))
            {
                tree.Insert(val);
                treeHeight = TreeHeight(tree.Root);
                nodeCount = 0;
                SetNodePositions();
                treePanel.Invalidate();
            }
            else
            {
                MessageBox.Show("Entered invalid value!", "Error!", MessageBoxButtons.OK);
            }
            textNewVal.Text = String.Empty;
        }

        private void treePanel_Paint(object sender, PaintEventArgs e)
        {
            if (tree.Root != null)
            {
                GraphicsContainer gc = e.Graphics.BeginContainer(
                    new Rectangle(20, 20, treePanel.Width - 40, treePanel.Height - 40),
                    new Rectangle(0, 0, treePanel.Width, treePanel.Height),
                    GraphicsUnit.Pixel);
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                DrawThree(e.Graphics, tree.Root);
                e.Graphics.EndContainer(gc);
            }
        }

        private void SetNodePositions()
        {
            int depth = 0;
            InorderTraverseAndSetPos(tree.Root, depth);
        }

        private void InorderTraverseAndSetPos(BSTDrawNode node, int depth)
        {
            if(node != null)
            {
                InorderTraverseAndSetPos(node.Left, depth + 1);
                node.XPos = nodeCount++;
                node.YPos = depth;
                InorderTraverseAndSetPos(node.Right, ++depth);

            }
        }

        private void DrawThree(Graphics g, BSTDrawNode root)
        {
            float dx = 0, dy = 0, dx2 = 0, dy2 = 0;
            float xScale;
            if (nodeCount == 1)
                xScale = treePanel.Width / 2;
            else
                xScale = (treePanel.Width / (nodeCount - 1));
            float yScale = (treePanel.Height) / (treeHeight + 1);
            if(root != null)
            {
                if (nodeCount == 1)
                    dx = xScale;
                else
                    dx = root.XPos * xScale;
                dy = root.YPos * yScale;
                string valStr = root.Value.ToString();
                Size vSz = TextRenderer.MeasureText(valStr, treePanel.Font);

                if (root.Left != null)
                {
                    dx2 = root.Left.XPos * xScale;
                    dy2 = root.Left.YPos * yScale;
                    g.DrawLine(Pens.Black, dx, dy, dx2, dy2);
                }
                if (root.Right != null)
                {
                    dx2 = root.Right.XPos * xScale;
                    dy2 = root.Right.YPos * yScale;
                    g.DrawLine(Pens.Black, dx, dy, dx2, dy2);
                }
                g.FillEllipse(Brushes.Brown, new RectangleF(dx - 18F, dy - 18F, 36F, 36F));
                g.DrawString(root.Value.ToString(), treePanel.Font, Brushes.White, new PointF(dx - (vSz.Width / 2)+1, dy - (vSz.Height / 2)-1));
                DrawThree(g, root.Left);
                DrawThree(g, root.Right);
            }
        }

        private int TreeHeight(BSTDrawNode node)
        {
            if(node == null) return -1;
            return 1 + Math.Max(TreeHeight(node.Left), TreeHeight(node.Right));
        }
    }
}
