﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTVisualizer
{
    public class BSTDrawNode
    {
        public BSTDrawNode(int val)
        {
            Value = val;
        }
        public int Value { get; set; }
        public float XPos{ get; set; }
        public float YPos{ get; set; }
        public BSTDrawNode Left { get; set; } = null;
        public BSTDrawNode Right { get; set; } = null;
    }

    public class BSTDraw
    {
        private BSTDrawNode root;
        public BSTDrawNode Root => root;

        public void Insert(int val)
        {
            if(root == null)
            {
                root = new BSTDrawNode(val);
                return;
            }
            BSTDrawNode newNode = new BSTDrawNode(val);
            BSTDrawNode currNode = root;
            while (true)
            {
                if(val < currNode.Value) // left
                {
                    if (currNode.Left != null)
                        currNode = currNode.Left;
                    else
                    {
                        currNode.Left = newNode;
                        break;
                    }
                } else // right
                {
                    if (currNode.Right != null)
                        currNode = currNode.Right;
                    else
                    {
                        currNode.Right = newNode;
                        break;
                    }
                }
            }
        }

        public void Clear()
        {
            root = null;
        }
    }
}
